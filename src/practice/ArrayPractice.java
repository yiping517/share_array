package practice;

//求數組元素的最大值
public class ArrayPractice {

	public static void main(String[] args) {
		int[] arr = new int[10];
		for(int i=0; i<arr.length; i++) {
			arr[i] = (int)(Math.random()*100);
			System.out.print( arr[i] + " ");
		}
		
		//1.令arr[0]是最大值
		int max = arr[0];
		
		//2.比較
		for(int i=0; i<arr.length; i++) {
			if(arr[i]>max) {
				max = arr[i];
			}
		}
		System.out.println();
		System.out.print("最大值是:"+max);
	}

}
