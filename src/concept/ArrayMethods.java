package concept;

import java.util.Arrays;

public class ArrayMethods {

	public static void main(String[] args) {
		//一、數組的複製
		//(一)方式一
		int[] source = {10,20,30,40,50};
		int[] destination = new int[6];//0,0,0,0,0,0
		
		//source:源數組
		//1:源數組的起始下標
		//destination:目標數組
		//0:目標數組的起始下標
		//4:要複製的元素個數
		System.arraycopy(source, 1, destination, 0, 4);
		
		for(int i=0; i<destination.length; i++) {
			System.out.print(destination[i]+" ");
		}
		
		
		//(方式二)
		int[] source1 = {10,20,30,40,50};
		int[] destination2 =Arrays.copyOf(source1, 6);
		for(int i=0; i<destination2.length; i++) {
			System.out.print(destination2[i]+" ");
		}
		
		
		//方式一比較有彈性、效率較高。
		//但擴充長度時,只有方式二做得到
		//二、擴充
		//數組一經創建，長度固定。擴充是指向一個新的對象
		int[] source3 = {10,20,30,40,50};
		source3 = Arrays.copyOf(source3, source3.length+1);
		for(int i=0; i<source3.length; i++) {
			System.out.print(source3[i]+" ");
		}
		
		//三、數組的排序
		int[] arr = {5,80,56,99,16,74};
		Arrays.sort(arr);
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
	}

}
