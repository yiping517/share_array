package concept;

public class ArrayDemo {

	public static void main(String[] args) {
		//1.數組的定義
		int[] arr = new int[5];
		//聲明整型數組arr,包含5個元素
		//每個元素都是int型,默認值為0
		System.out.println(arr[3]);
		
		//2.數組初始化的方式 : 
		int[] arr2 = new int[4]; //0,0,0,0
		int[] arr3 = {1,4,6,8};
		int[] arr4 = new int[] {1,4,6,8};
//		int[] arr5 = new int[4] {1,4,6,8};
		int[] arr6;
//		arr6 = {1,4,6,8}; //編譯錯誤，此方式只能聲明同時初始化
		arr6 =  new int[] {1,4,6,8};
		
		//3.數組的訪問
		System.out.println(arr6.length);//數組長度
		arr6[0]=100;//賦值
//		arr6[4]=200;
//		System.out.println(arr6[4]);
		
		//4.數組的遍歷
		int[] arr7 = new int[10];
		for(int i=0; i<arr.length; i++) {
			arr[i] = (int)(Math.random()*100);
		}
		
		System.out.println("正序輸出");
		for(int i=0; i<arr.length; i++) {
			System.out.println(arr[i]);
		}
		
		System.out.println("倒序輸出");
		for(int i=arr.length-1; i>=0; i--) {
			System.out.println(arr[i]);
		}
	}

}
